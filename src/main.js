import Vue from 'vue'
import App from './App.vue'
import router from './router';

import BootstrapVue from 'bootstrap-vue';
import {store} from './store/store.js';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './assets/style.css';
import './plugins/axios';

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
	router: router,
	store,
	render: h => h(App),
}).$mount('#app')
