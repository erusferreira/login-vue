import Vue from 'vue';
import Router from 'vue-router';

import Login from './components/login/loginComponent.vue';
import SignUp from './components/login/signUpComponent.vue'; 
import Lista from './components/lista/listaComponent.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', component: Login },
        { path: '/sign-up', component: SignUp },
        { path: '/lista', component: Lista }
    ]
})