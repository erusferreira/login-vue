import Vue from 'vue';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/';

Vue.use({
    install(Vue) {
        Vue.prototype.$http = axios;
        Vue.prototype.$http.interceptors.request.use((config) => {
            const token = localStorage.getItem('userToken');
            if (token) {
                config.headers.authorization = `Bearer ${token}`
            }
            return config
        }, (err) => {
            return Promise.reject(err)
        });
        
        Vue.prototype.$http.interceptors.response.use((response) => {
            return response;
        }, (error) => {
            if (error.response.status === 403 && window.location.pathname !== '/') {
                window.location = '/';
            }            
            return Promise.reject(error)
        })
        
    }
})

