import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        users: [],
        saveInLocalStorage: true
    },
    mutations: {
        logout() {
            localStorage.removeItem('userToken');
        }
    },
    actions: {
        saveToken(context, payload) {
            localStorage.setItem('userToken', payload);
        }
    }
});
