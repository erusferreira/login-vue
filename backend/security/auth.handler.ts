import * as restify from 'restify';
import { NotAuthorizedError } from 'restify-errors';
import * as jwt from 'jsonwebtoken';

import { User } from '../users/users.model';
import { environment } from '../common/environment';

export const authenticate: restify.RequestHandler = (req, resp, next) => {
    const { email, password } = req.body;

    User.findByEmail(email, '+password')
        .then((user: any) => {
            if (user && user.matches(password)) {
                const token = jwt.sign({
                    sub: user.email,
                    iss: 'login-vue'
                }, environment.security.apiSecret)
                resp.json({
                    firstName: user.firstName,
                    email: user.email,
                    accessToken: token
                });
                return next(false);
            } else {
                return next(new NotAuthorizedError('Usuário não autorizado!'));
            }
    }).catch(next);

}