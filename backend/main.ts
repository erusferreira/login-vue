import { Server } from './server/server';
import { usersRouter } from './users/users.router';

const server = new Server();

server.bootstrap([usersRouter]).then(server => {
        console.log(`server is listening on: ${server.application.address().port}`);
    }).catch(error  => {
        console.log(`server failing to start`);
        console.error(error);
        // codigo 1 = saída anormal.
        process.exit(1);
    })