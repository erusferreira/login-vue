import * as restify from 'restify';
import { NotFoundError } from 'restify-errors';

import { Router } from '../common/router';
import { User } from './users.model';
import { authenticate } from '../security/auth.handler';
import { authorize } from '../security/authz.handler';

class UsersRouter extends Router {
    
    constructor() {
        super();
        this.on('beforeRender', document => {
            document.password = undefined;
        });
    }

    applyRoutes(application: restify.Server) {
        application.get('/users', [authorize(), (req: restify.Request, resp: restify.Response, next: restify.Next) => {
            User.find()
                .then(this.render(resp, next))
                .catch(next);
        }]);

        application.get('/users/:id', (req: restify.Request, resp: restify.Response, next: restify.Next) => {
            User.findById(req.params.id)
                .then(this.render(resp, next))
                .catch(next);
        });

        application.post('/users', (req: restify.Request, resp: restify.Response, next: restify.Next) => {
            let user = new User(req.body);
            user.save()
                .then(this.render(resp, next))
                .catch(next);
        });

        application.put('/users/:id', (req: restify.Request, resp: restify.Response, next: restify.Next) => {
            const options = {runValidators: true, overwrite: true};
            User.update({_id: req.params.id}, req.body, options)
                .exec().then((result): any => {
                    if(result.n) {
                        return User.findById(req.params.id);
                    } else {
                        throw new NotFoundError('Documento não encontrado.');
                    }
                }).then(this.render(resp, next))
                  .catch(next);
        });

        application.patch('users/:id', (req: restify.Request, resp: restify.Response, next: restify.Next) => {
            const options = {runValidators: true, new: true};
            User.findOneAndUpdate(req.params.id, req.body, options)
                .then(this.render(resp, next))
                .catch(next);
        });

        application.del('/users/:id', (req: restify.Request, resp: restify.Response, next: restify.Next) => {
            User.remove({_id: req.params.id})
                .exec()
                .then((cmdResult: any) => {
                    if (cmdResult.result.n) {
                        resp.send(204);
                    } else{
                        throw new NotFoundError('Documento não encontrado.');
                    }
                    return next();
                })
                .catch(next);
        });

        application.post('/users/authenticate', authenticate);
    }
}

export const usersRouter = new UsersRouter();
