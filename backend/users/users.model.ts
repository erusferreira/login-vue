import * as mongoose from 'mongoose';
var bcrypt = require('bcryptjs');

import { environment } from '../common/environment';

export interface UserInterface extends mongoose.Document {
    firstName: string;
    lastName: string;
    gender: string;
    email: string;
    password: string;
    matches(password: string): boolean;
}

export interface UserModel extends mongoose.Model<UserInterface> {
    findByEmail(email: string, projection?: string): Promise<UserInterface>
  }
  

const userSchema = new mongoose.Schema({
    firstName: { type: String, minlength: 2, maxlength: 80, required: true },
    lastName: { type: String, minlength: 2, maxlength: 80, required: true },
    gender: { type: String, enum: ['masculino', 'feminino'], required: false },
    email: { 
        type: String, 
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 
        unique: true, required: true },
        password: { type: String, select: false, required: true } 
});

userSchema.statics.findByEmail = function(email: string, projection: string ) {
    return this.findOne({email}, projection) //{email: email}
}

userSchema.methods.matches = function(password: string): boolean {
    return bcrypt.compareSync(password, this.password);
}

const hashPassword = (obj: UserInterface, next: any)=>{
    bcrypt.hash(obj.password, environment.security.saltRounds)
    .then((hash: any )=>{
        obj.password = hash
        next()
    }).catch(next)
}

const saveMiddleware = function (this: UserInterface, next: any){
    const user = this
    if(!user.isModified('password')){
        next()
    }else{
        hashPassword(user, next)
    }
}

const updateMiddleware = function (this: mongoose.Query<UserInterface>, next:any){
    if(!this.getUpdate().password){
        next()
    }else{
        hashPassword(this.getUpdate(), next)
    }
}

userSchema.pre('save', saveMiddleware)
userSchema.pre('findOneAndUpdate', updateMiddleware)
userSchema.pre('update', updateMiddleware)

export const User = mongoose.model<UserInterface, UserModel>('User', userSchema);
