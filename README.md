# Login Vuejs

### Running Backend on development mode
Go to /backend path;
Install the application: `npm i`;
Make sure you have typescript globally installed (`npm install -g typescript`)
and then run: `tsc -w` to transpile the typescript files in javascript.  
Open another instance of terminal and run `npm run start`;  
The server will be listening on: http://localhost:3000/  

If you receive an error message complaining about Python not found and you are on Windows OS, Run this command:
`npm --add-python-to-path='true' --debug install --global windows-build-tools`;

### Running FrontEnd on development mode
Go to the root folder;  
Install the application: `npm i`;
Run `npm run serve`;  
Open you browser on: http://localhost:8080/  

### Compiles and minifies for production
```
npm run build
```